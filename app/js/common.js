const TEFIT = {}

TEFIT.products = [{
  "category": "Сытные пироги",
  "list": [{
    "ID": 1,
    "title": "Пирог с ветчиной и сыром",
    "image": [
      "../img/pirog-vetch-sir.png",
      "../img/pirog-vetch-sir2.png"
    ],
    "weight": 1,
    "price": 760,
    "ingredients": [
      "Ветчина",
      "Сыр"
    ],
    "searchIngredients": [
      "Мясо",
      "Сыр"
    ],
    "description": "Наш фирменный пирог с ветчиной и сыром - идеально для завтрака"
  }, {
    "ID": 2,
    "title": "Пирог с говядиной и капустой",
    "image": [
      "../img/pirog-vetch-sir.png",
      "../img/pirog-vetch-sir4.png"
    ],
    "weight": 1,
    "price": 560,
    "ingredients": [
      "Говядина",
      "Капуста"
    ],
    "searchIngredients": [
      "Мясо",
      "Овощи"
    ],
    "description": "Оригинальный рецепт, с пылу с жару - пирог с говядиной и капустой"
  }, {
    "ID": 3,
    "title": "Пирог с говядиной и картофелем",
    "image": [
      "../img/pirog-vetch-sir.png",
      "../img/pirog-vetch-sir4.png"
    ],
    "weight": 1,
    "price": 560,
    "ingredients": [
      "Говядина",
      "Картофель"
    ],
    "searchIngredients": [
      "Мясо",
      "Овощи"
    ],
    "description": "Оригинальный рецепт, с пылу с жару - пирог с говядиной и картофелем"
  }, {
    "ID": 4,
    "title": "Пирог с говядиной и луком",
    "image": [
      "../img/pirog-vetch-sir.png",
      "../img/pirog-vetch-sir4.png"
    ],
    "weight": 1,
    "price": 650,
    "ingredients": [
      "Говядина",
      "Лук"
    ],
    "searchIngredients": [
      "Мясо"
    ],
    "description": "Оригинальный рецепт, с пылу с жару - пирог с говядиной и луком"
  }, {
    "ID": 5,
    "title": "Пирог с капустой",
    "image": [
      "../img/pirog-vetch-sir.png",
      "../img/pirog-vetch-sir4.png"
    ],
    "weight": 1,
    "price": 380,
    "ingredients": [
      "Капуста"
    ],
    "searchIngredients": [
      "Овощи"
    ],
    "description": "Наш фирменный пирог с капустой, пойдет для вегетарианцев"
  }, {
    "ID": 6,
    "title": "Пирог с капустой и грибами",
    "image": [
      "../img/pirog-vetch-sir.png",
      "../img/pirog-vetch-sir4.png"
    ],
    "weight": 1,
    "price": 760,
    "ingredients": [
      "Капуста",
      "Грибы"
    ],
    "searchIngredients": [
      "Овощи",
      "Грибы"
    ],
    "description": "Наш фирменный пирог с капустой и грибами, пойдет для вегетарианцев"
  }, {
    "ID": 7,
    "title": "Пирог с картофелем и луком",
    "image": [
      "../img/pirog-vetch-sir.png",
      "../img/pirog-vetch-sir4.png"
    ],
    "weight": 1,
    "price": 320,
    "ingredients": [
      "Картофель",
      "Лук"
    ],
    "searchIngredients": [
      "Овощи"
    ],
    "description": "Наш фирменный пирог с картофелем и луком, пойдет для вегетарианцев"
  }  , {
    "ID": 8,
    "title": "Пирог с курицей и грибами",
    "image": [
      "../img/pirog-vetch-sir.png",
      "../img/pirog-vetch-sir4.png"
    ],
    "weight": 1,
    "price": 710,
    "ingredients": [
      "Курица",
      "Грибы"
    ],
    "searchIngredients": [
      "Курица",
      "Грибы"
    ],
    "description": "Пирог с курицей и грибами"
  }, {
    "ID": 9,
    "title": "Пирог с курицей и рисом",
    "image": [
      "../img/pirog-vetch-sir.png",
      "../img/pirog-vetch-sir4.png"
    ],
    "weight": 1,
    "price": 690,
    "ingredients": [
      "Курица",
      "Рис"
    ],
    "searchIngredients": [
      "Курица"
    ],
    "description": "Классический пирог прямо из печи с курицей и рисом. Подойдет для обеденного перерыва"
  }]
}, {
  "category": "Сладкие пироги",
  "list": [{
    "ID": 10,
    "title": "Пирог с брусникой",
    "image": [
      "../img/pirog-vetch-sir.png"
    ],
    "weight": 1,
    "price": 640,
    "ingredients": [
      "Брусника"
    ],
    "searchIngredients": [
      "Ягоды"
    ],
    "description": "Лёгкий и одновременно нежный пирог с брусникой"
  }, {
    "ID": 11,
    "title": "Пирог с вишней",
    "image": [
      "../img/pirog-vetch-sir.png"
    ],
    "weight": 1,
    "price": 640,
    "ingredients": [
      "Вишня"
    ],
    "searchIngredients": [
      "Ягоды"
    ],
    "description": "Сладкий пирог с вишней - идеально для любителей традиционного чаепития"
  }]
}];

TEFIT.navCategory = document.querySelector('.top-panel');
TEFIT.navIngredients = document.querySelector('.nav_ingredients')
TEFIT.listProducts = document.querySelector('.category_list');
TEFIT.modalDialog = document.querySelector('.modalDialog');
TEFIT.slider = document.querySelector('.slider');

if (!TEFIT.__proto__.forEach) {

  TEFIT.__proto__.forEach = function(callback, thisArg) {

    var T, k;

    if (this === null) {
      throw new TypeError('this is null or not defined');
    }

    var O = Object(this);

    var len = O.length >>> 0;

    if (typeof callback !== 'function') {
      throw new TypeError(callback + ' is not a function');
    }

    if (arguments.length > 1) {
      T = thisArg;
    }

    k = 0;

    while (k < len) {

      var kValue;

      if (k in O) {

        kValue = O[k];

        callback.call(T, kValue, k, O);
      }
      k++;
    }
  };
}

function getCategory(category) {
  let sec,      // section.caregory
      h1,       // h1.category_title 
      divItem,  // div.category_item">
      img,      // <img src="">
      h4,       // h4.category_item_title
      divValue, // div.category_value
      wt,       // span.wt
      price,    // span.price
      frag = document.createDocumentFragment();

  TEFIT.products.forEach(function (e, i) {
    if (e.category == category || !category) {
      sec = document.createElement('section');
      sec.classList.add('category');
      h1 = document.createElement('h1');
      h1.classList.add('category_title');
      h1.innerHTML = e.category;
      sec.appendChild(h1);
      frag.appendChild(sec);
      e.list.forEach(function(e, i) {
        divItem = document.createElement('div');
        divItem.classList.add('category_item');
        divItem.setAttribute('data-id', e.ID);
        img = document.createElement('img');
        img.src = e.image[0];
        h4 = document.createElement('h4');
        h4.classList.add('category_item_title');
        h4.innerHTML = e.title;
        divValue = document.createElement('div');
        divValue.classList.add('category_value');
        wt = document.createElement('span');
        wt.classList.add('wt');
        wt.innerHTML = 'Вес: ' + e.weight + 'кг';
        price = document.createElement('span');
        price.classList.add('price');
        price.innerHTML = e.price + ' руб.';
        divValue.appendChild(wt);
        divValue.appendChild(price);
        divItem.appendChild(img);
        divItem.appendChild(h4);
        divItem.appendChild(divValue);
        sec.appendChild(divItem);
    });
    }
  });
  return frag;
}

function getIngredients(ingredient) {
  let sec,      // section.caregory
      h1,       // h1.category_title 
      divItem,  // div.category_item">
      img,      // <img src="">
      h4,       // h4.category_item_title
      divValue, // div.category_value
      wt,       // span.wt
      price,    // span.price
      frag = document.createDocumentFragment();

  TEFIT.products.forEach(function (e, i) {
    if (e.category == 'Сытные пироги' || e.category == 'Сладкие пироги') {
      let isOk = false;
      sec = document.createElement('section');
      sec.classList.add('category');
      h1 = document.createElement('h1');
      h1.classList.add('category_title');
      h1.innerHTML = e.category;
      e.list.forEach(function(e, i) {
        if (e.searchIngredients.includes(ingredient)) {
          divItem = document.createElement('div');
          divItem.classList.add('category_item');
          divItem.setAttribute('data-id', e.ID);
          img = document.createElement('img');
          img.src = e.image[0];
          h4 = document.createElement('h4');
          h4.classList.add('category_item_title');
          h4.innerHTML = e.title;
          divValue = document.createElement('div');
          divValue.classList.add('category_value');
          wt = document.createElement('span');
          wt.classList.add('wt');
          wt.innerHTML = 'Вес: ' + e.weight + 'кг';
          price = document.createElement('span');
          price.classList.add('price');
          price.innerHTML = e.price + ' руб.';
          divValue.appendChild(wt);
          divValue.appendChild(price);
          divItem.appendChild(img);
          divItem.appendChild(h4);
          divItem.appendChild(divValue);
          sec.appendChild(divItem);
          isOk = true;
        }
      });
      if (isOk) {
        sec.insertBefore(h1, sec.firstChild);
        frag.appendChild(sec);
      }
    }
  });
  return frag;
}

function removeChildrenRecursively(node)
{
  if (!node) return;
  while (node.hasChildNodes()) {
    removeChildrenRecursively(node.firstChild);
    node.removeChild(node.firstChild);
  }
}

function refreshListProducts(c, v) {
  removeChildrenRecursively(TEFIT.listProducts);
  if (c == 'category') {
    TEFIT.listProducts.appendChild(getCategory(v));
  } else if (c == 'ingredients') {
    TEFIT.listProducts.appendChild(getIngredients(v));
  }
}

TEFIT.listProducts.appendChild(getCategory());

TEFIT.navCategory.addEventListener('click', function(e) {
  e.preventDefault();
  let c = 'category',
      v = e.target.innerHTML;
  if (e.target.tagName == "A" || e.target.tagName == "IMG") {
    TEFIT.slider.style.display = 'none';
    if (e.target.tagName == "IMG") {
      if (window.matchMedia('(min-width : 900px)').matches) {
        TEFIT.slider.style.display = 'block';
      }
    }
    TEFIT.navCategory.querySelectorAll('li').forEach(function(e) {
        e.classList.remove('hover');
    });
    if (e.target.parentNode.tagName == "LI") {
      e.target.parentNode.classList.add('hover');
    }
  }
  switch (v) {
    case 'Сытные пироги':
      refreshListProducts(c, v);
      break;
    case 'Сладкие пироги':
      refreshListProducts(c, v);
      break;
    case 'Пирожки':
      refreshListProducts(c, v);
      break;
    case 'Напитки':
      refreshListProducts(c, v);
      break;
    default:
      if (e.target.tagName == "IMG" || e.target.classList[0] == 'logo_link') {
        refreshListProducts(c);
      }
      break;
  }
});

TEFIT.navIngredients.addEventListener('click', function(e) {
  e.preventDefault();
  let c = 'ingredients',
      v = e.target.innerHTML;
  TEFIT.slider.style.display = 'none';
  TEFIT.navCategory.querySelectorAll('li').forEach(function(e) {
      e.classList.remove('hover');
  });
  switch (v) {
    case 'Мясо':
      refreshListProducts(c, v);
      break;
    case 'Курица':
      refreshListProducts(c, v);
      break;
    case 'Рыба':
      refreshListProducts(c, v);
      break;
    case 'Сыр':
      refreshListProducts(c, v);
      break;
    case 'Овощи':
      refreshListProducts(c, v);
      break;
    case 'Грибы':
      refreshListProducts(c, v);
      break;
    case 'Ягоды':
      refreshListProducts(c, v);
      break;
    case 'Фрукты':
      refreshListProducts(c, v);
      break;
    case 'Творог':
      refreshListProducts(c, v);
      break;
    default:
      break;
  }
});

TEFIT.listProducts.addEventListener('click', function(e) {
  let target = e.target;
  if (target.classList.value != 'category') {
    while (target.classList.value != 'category_item') {
      target = target.parentNode;
    }
    removeChildrenRecursively(TEFIT.modalDialog);
    let frag = document.createDocumentFragment();

    let modalTitle,
        modalDescription,
        modalItemDescription1,
        modalItemDescription2,
        modalBtn,
        modalIngredients,
        modalImg,
        modalImageMain,
        modalImage,
        modalImage0,
        modalImage1,
        modalImage2,
        modalPrice,
        modalWeight,
        modalClose,
        modalImageList,
        modalImageListLi,
        modalWrapper,
        ID = target.getAttribute('data-id');
        catSelect = target.parentNode.querySelector('.category_title').innerHTML;

    TEFIT.products.forEach(function(e) {
      if (catSelect == e.category) {
        e.list.forEach(function(e, i){
          if (e.ID == ID) {

            modalImg = document.createElement('section');
            modalImg.classList.add('modal_img');

            modalImageMain = document.createElement('img');
            modalImageMain.classList.add('modal-main-img');
            modalImageMain.src = e.image[0];

            modalImg.appendChild(modalImageMain);

            modalImageList = document.createElement('ul');
            modalImageList.classList.add('modal-img-list');

            e.image.forEach(function(e, i) {
              modalImageListLi = document.createElement('li');
              modalImage = document.createElement('img');
              modalImage.classList.add('modal_img' + i);
              modalImage.src = e;
              modalImageListLi.appendChild(modalImage);
              modalImageList.appendChild(modalImageListLi);
            });
            modalImg.appendChild(modalImageList);


            modalItemDescription1 = document.createElement('section');
            modalItemDescription1.classList.add('modal_item_description');

            modalTitle = document.createElement('h2');
            modalTitle.classList.add('modal_category_title');
            modalTitle.innerHTML = e.title;

            modalItemDescription1.appendChild(modalTitle);

            modalDescription = document.createElement('p');
            modalDescription.classList.add('modal_category_description');
            modalDescription.innerHTML = e.description;

            modalItemDescription1.appendChild(modalDescription);


            modalItemDescription2 = document.createElement('section');
            modalItemDescription2.classList.add('modal_item_description');
            modalItemDescription2.classList.add('max');

            modalWeight = document.createElement('div');
            modalWeight.classList.add('modal_weight');
            modalWeight.innerHTML = e.weight + ' кг.';

            modalItemDescription2.appendChild(modalWeight);

            modalIngredients = document.createElement('div');
            modalIngredients.classList.add('modal_ingredients');
            modalIngredients.innerHTML = e.ingredients.join(', ');

            modalItemDescription2.appendChild(modalIngredients);

            modalPrice = document.createElement('div');
            modalPrice.classList.add('modal_price');
            modalPrice.innerHTML = e.price + ' руб.';

            modalItemDescription2.appendChild(modalPrice);

            modalBtn = document.createElement('a');
            modalBtn.classList.add('modal_btn');
            modalBtn.innerHTML = 'Закрыть';

            modalItemDescription2.appendChild(modalBtn);

            modalClose = document.createElement('div');
            modalClose.classList.add('modal-wrapper-after');

            modalWrapper = document.createElement('div');
            modalWrapper.classList.add('modal-wrapper');

            modalWrapper.appendChild(modalImg);
            modalWrapper.appendChild(modalItemDescription1);
            modalWrapper.appendChild(modalItemDescription2);
            modalWrapper.appendChild(modalClose);

            TEFIT.modalDialog.appendChild(modalWrapper);
            TEFIT.modalDialog.style.display = 'block';
          }
        });
      }
    });
  }
});

TEFIT.modalDialog.addEventListener('click', function(e) {
  let value = e.target.classList.value,
      mainImg = TEFIT.modalDialog.querySelector('.modal-main-img');
  if (value == 'modal-wrapper-after' || value == 'modal_btn') {
    TEFIT.modalDialog.style.display = 'none';
  } else if (e.target.tagName == 'IMG' && value != 'modal-main-img') {
    mainImg.src = e.target.src;
  }
});

function fixNav() {
  if (window.matchMedia('(min-width : 900px)').matches) {
    let cat = TEFIT.navCategory,
        ing = TEFIT.navIngredients;

    let catDot = cat.getBoundingClientRect();
    let ingDot = ing.getBoundingClientRect();
    let body = document.querySelector('.hero');
    let categ = document.querySelector('.category_list');

    if (cat.classList.contains('fixed') && window.pageYOffset <= catDot.top) {
      cat.classList.remove('fixed');
      body.style.marginTop = "0px";
    } else if (window.pageYOffset > catDot.top) {
      cat.classList.add('fixed');
      cat.style.cssText = "top: 0;"
      body.style.marginTop = catDot.bottom + "px";   
    } // Иногда, страница прокручивается вверх (прыгает) и навигация
      // по ингридиентам остаётся в фиксированном положении,
      // разделение на 2 условия решает проблему с навигацией, но не прыжками.
      // Возможно, проблема только на тестах (включен автопрефиксер).
    if (ing.classList.contains('fixed') && body.getBoundingClientRect().bottom > ingDot.top - 10) {
        ing.classList.remove('fixed');
        categ.style.marginTop = "0px";        
    } else if (window.pageYOffset > window.pageYOffset + ingDot.top - catDot.bottom) {
      ing.classList.add('fixed');
      ing.style.cssText = "top: " + catDot.bottom + "px;";
      categ.style.marginTop = (ingDot.bottom - 10) + "px";
    }
  }
}

window.onscroll = function() {
  fixNav();
};